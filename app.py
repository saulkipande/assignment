from flask import Flask
from flask_pymongo import PyMongo
from bson import json_util
from bson.json_util import dumps
from flask_celery import make_celery
app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'redis://localhost//'
app.config['CELERY_RESULT_BACKEND'] = 'mongodb://localhost:27017/maradb'
app.config["MONGO_URI"] = "mongodb://localhost:27017/maradb"

celery = make_celery(app)
mongo = PyMongo(app)
@app.route('/hello_world')
def hello_world():
    get_products.delay()
    return get_products()
#GET PRODUCTS
@celery.task(name="app.get_products")
def get_products():
    products = mongo.db.products.find()
    return dumps(products)
#get product id
@app.route('/product/<int:id>')
def get_a_product(id):
    product = {}
    for item in products:
        if item ['id'] == id:
            product = {
                'name':item['name'],
                'price':item['price'],
                'quantity':item['quantity']
            }
            return dumps(product)
@app.route('/add_user')
def add_user():
    user = mongo.db.users
    user.insert({'name':'anthony'})
    user.insert({'name':'saul'})
    user.insert({'name':'obed'})
    user.insert({'name':'Grace'})
    return 'Added user'
@app.route('/find')
def find():
    user = mongo.db.users
    obed = user.find_one({'name':'obed'})
    return 'you found'+ obed
    


if __name__ == "__main__":
    app.run(debug=True)