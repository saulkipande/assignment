from flask import Flask, jsonify, request, Response, json
from flask_pymongo import PyMongo
from bson import json_util
from bson.json_util import dumps

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/maradb"
mongo = PyMongo(app)
 
@app.route('/')
def hello_world():
    return 'Welcome to Emie Bakery!'


# GET Products
@app.route('/products')
def get_products():
    products = mongo.db.products.find()
    return dumps(products)

@app.route('/users')
def get_users():
    users = mongo.db.users.find()
    return dumps(users)

# GET product id
@app.route('/products')
def get_a_product(name):
    product = {}
    for item in products:
        if item['name'] == name:
            product = {
                'name': item['name'],
                'price': item['price'],
                'quantity':item['quantity']
            }
    return jsonify(product)






app.run(port=5000,debug=True)